# OpenML dataset: Drug-Classification

https://www.openml.org/d/43382

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Since as a beginner in machine learning it would be a great opportunity to try some techniques to predict the outcome of the drugs that might be accurate for the patient. 
Content
The target feature is

Drug type

The feature sets are:

Age
Sex
Blood Pressure Levels (BP)
Cholesterol Levels
Na to Potassium Ration

Inspiration
The main problem here in not just the feature sets and target sets but also the approach that is taken in solving these types of problems as a beginner. So best of luck.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43382) of an [OpenML dataset](https://www.openml.org/d/43382). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43382/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43382/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43382/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

